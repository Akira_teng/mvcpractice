﻿using AutoMapper;

namespace MvcPractice.Mappers.Profiles
{
    public class BaseProfile : Profile
    {
        public sealed override string ProfileName => GetType().Name;
    }
}