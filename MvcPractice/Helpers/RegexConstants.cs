﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Helpers
{
    public class RegexConstants
    {
        /// <summary>
        /// email
        /// </summary>
        public const string Email = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,4}\.[0-9]{1,4}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        /// <summary>
        /// 手機
        /// </summary>
        public const string CellPhone = @"^[0][9][0-9]{8}$";

        /// <summary>
        /// 家用電話, 範例：02-25575918#100
        /// </summary>
        public const string TelPhone = @"^(\\(?\\d\\d\\)?)?( |-|\\.)?(\\d{3,4})( |-|\\.)?\\d{4,4}(( |-|\\.)?[#]+ ?\\d+)?$";


    }
}