﻿using MvcPractice.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcPractice.Models.Interface
{
    interface ITodosRepository : IRepository<Todos>
    {
    }
}
