﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcPractice.Models.FormBeans
{
    public class LoginBean
    {
        [Required(ErrorMessage = "請輸入帳號", AllowEmptyStrings = false)]
        [Display(Name = "帳號(Email)")]
        public string Email { get; set; }

        [Required(ErrorMessage = "請輸入密碼", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Display(Name = "密碼")]
        public string Password { get; set; } 
        
        [Display(Name = "驗證碼")]
        public string ValidateCode { get; set; }
    }
}