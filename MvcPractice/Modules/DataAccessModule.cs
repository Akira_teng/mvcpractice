﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using MvcPractice.Helpers;
using MvcPractice.Models.Context;

namespace MvcPractice.Modules
{
    public class DataAccessModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //            builder.RegisterType<SolventoContextFactory>().Keyed<IContextFactory>(ContextEnum.SolventoContext);

            builder.Register<Func<ContextEnum, IContextFactory>>(c => s => c.ResolveKeyed<IContextFactory>(s));

            //builder.RegisterType(typeof(WGIPEntities)).As(typeof(DbContext)).InstancePerLifetimeScope();
            //builder.RegisterType<GenericUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Repositories"))
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Services"))
                .AsImplementedInterfaces();
        }


    }
}