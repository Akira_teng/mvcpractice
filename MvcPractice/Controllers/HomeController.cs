﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MvcPractice.Models;
using MvcPractice.Models.FormBeans;
using MvcPractice.Models.Services;
using PagedList;

namespace MvcPractice.Controllers
{
    public class HomeController : Controller
    {
        AccountService accountService = new AccountService();
        TodoService todoService = new TodoService();
        
        private readonly IMapper _mapper;

        public HomeController(IMapper mapper)
        {
            _mapper = mapper;
        }
        
        public ActionResult Index(int? page)
        {            
            if (Session["userid"] != null)    
            {
                var pageNumber = page ?? 1;
                Users user = accountService.GetUserById(Convert.ToInt32(Session["userId"]));
                TodoListBean userTodoList = todoService.GetUserTodoList(user);
                userTodoList.todoThingsPaged = userTodoList.todoThingsList.ToPagedList(pageNumber, 5);

                return View(userTodoList);
            }
            else   
            {
                return RedirectToAction("Index", "Login");
            }
        }

        [HttpPost]
        public ActionResult Create(String content, String completed, int userid)
        {
            todoService.CreateNewTodoThing(content, completed, userid);

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult Edit(String content, String completed, int id)
        {
            todoService.EditTodoThing(content, completed, id);

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            todoService.DeleteTodoThing(id);

            return new EmptyResult();
        }
    }
}