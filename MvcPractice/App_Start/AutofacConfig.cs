﻿using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using MvcPractice.Mappers.Profiles;
using MvcPractice.Modules;

namespace MvcPractice
{
    public class AutofacConfig
    {
        public static void Initialize()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterModule(new DataAccessModule());
            builder.RegisterModule(new AttributeModule());
            builder.RegisterModule(new CommonHandlerModule());
            builder.RegisterModule(new AutoMapperModule());

            IContainer container = builder.Build();

            var profiles = container.Resolve<IEnumerable<BaseProfile>>();
            Mapper.Initialize(x =>
            {
                x.ConstructServicesUsing(container.Resolve);
                profiles.ToList().ForEach(x.AddProfile);
            });

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}