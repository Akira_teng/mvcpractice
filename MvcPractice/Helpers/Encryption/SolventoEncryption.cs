﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Helpers.Encryption
{
    public class SolventoEncryption : ISolventoEncryption
    {
        IEncryption _encryption;

        public SolventoEncryption(IEncryption encryption)
        {
            _encryption = encryption;
        }

        public SolventoDictionary<string, int> TranslatorToDictionary(string sourceStr)
        {
            SolventoDictionary<string, int> dicEncrypt = new SolventoDictionary<string, int>();

            try
            {
                if (sourceStr != null && sourceStr.Length > 0)
                {
                    sourceStr = _encryption.AesDecoding(sourceStr);

                    string[] sourceAry = sourceStr.Split('@');
                    foreach (string source in sourceAry)
                    {
                        string[] tempAry = source.Split('=');
                        dicEncrypt.Add(tempAry[0], int.Parse(tempAry[1]));
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dicEncrypt;
        }

        public string TranslatorToStr(SolventoDictionary<string, int> dicEncrypt)
        {
            string result = string.Empty;

            foreach (var item in dicEncrypt)
            {
                result += "@" + item.Key + "=" + item.Value.ToString();
            }

            if (!string.IsNullOrEmpty(result))
                result = result.Remove(0, 1);

            return _encryption.AesEncoding(result);
        }
    }
}