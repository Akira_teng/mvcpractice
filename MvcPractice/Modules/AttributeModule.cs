﻿using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPractice.Helpers.Encryption;

namespace MvcPractice.Modules
{
    public class AttributeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterFilterProvider();
            //            builder.Register(c => new ActionExecuteAttribute(c.Resolve<IRoleService>(), c.Resolve<ISolventoEncryption>()))
            //                .AsActionFilterFor<Controller>().InstancePerHttpRequest();
            //            builder.Register(c => new ErrorAttribute())
            //                .AsExceptionFilterFor<Controller>().InstancePerHttpRequest();
        }
    }
}