﻿using MvcPractice.Models;
using MvcPractice.Models.FormBeans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Mappers.Profiles
{
    public class TodoProfile : BaseProfile
    {
        public TodoProfile()
        {
            //FormBean to Entity
            //CreateMap<TodoListBean, Todos>()
            //    .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID))
            //    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password));

            //Entity to FormBean
            //CreateMap<Todos, TodoListBean>()
            //    .ForMember(dest => dest.userID, opt => opt.MapFrom(src => src.UserID))
            //    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password));
        }

    }
}