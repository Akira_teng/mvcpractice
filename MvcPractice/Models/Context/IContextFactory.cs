﻿using System;
using System.Data.Entity;

namespace MvcPractice.Models.Context
{
    public interface IContextFactory : IDisposable
    {
        DbContext Get();
    }
}
