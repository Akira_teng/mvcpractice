﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MvcPractice.Helpers.Encryption
{
    public class Encryption : IEncryption
    {
        public string AesEncoding(string sourceStr)
        {
            string result = string.Empty;
            try
            {
                byte[] content = Encoding.UTF8.GetBytes(sourceStr);
                byte[] key = Encoding.UTF8.GetBytes(Constants.EncrptKey);
                MD5CryptoServiceProvider provider_MD5 = new MD5CryptoServiceProvider();
                byte[] byte_pwdMD5 = provider_MD5.ComputeHash(key); //hash後使得長度固定
                RijndaelManaged provider_AES = new RijndaelManaged();
                ICryptoTransform encrypt_AES = provider_AES.CreateEncryptor(byte_pwdMD5, byte_pwdMD5);

                byte[] output = encrypt_AES.TransformFinalBlock(content, 0, content.Length);
                result = Convert.ToBase64String(output).Replace("+", " ");
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public string AesDecoding(string sourceStr)
        {
            if (sourceStr != null)
            {
                sourceStr = sourceStr.Replace(" ", "+");
            }
            string result = string.Empty;
            try
            {
                //加密字串轉Base64
                byte[] Content = Convert.FromBase64String(sourceStr);
                //key值轉GetBytes
                byte[] key = System.Text.Encoding.UTF8.GetBytes(Constants.EncrptKey);

                System.Security.Cryptography.MD5CryptoServiceProvider provider_MD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] byte_pwdMD5 = provider_MD5.ComputeHash(key);
                System.Security.Cryptography.RijndaelManaged provider_AES = new System.Security.Cryptography.RijndaelManaged();
                System.Security.Cryptography.ICryptoTransform decrypt_AES = provider_AES.CreateDecryptor(byte_pwdMD5, byte_pwdMD5);
                byte[] decrypt = decrypt_AES.TransformFinalBlock(Content, 0, Content.Length);
                result = System.Text.Encoding.UTF8.GetString(decrypt);
            }
            catch (Exception e)
            {
            }
            return result;
        }
    }
}