﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcPractice.Helpers.Encryption
{
    public interface ISolventoEncryption
    {
        /// <summary>
        /// 將字串轉換成Dictionary
        /// EX: pid=1@nid=2 轉換成 Dictionary
        /// </summary>
        /// <param name="sourceStr"></param>
        /// <returns></returns>
        SolventoDictionary<string, int> TranslatorToDictionary(string sourceStr);

        /// <summary>
        /// 將Dictionary轉換成字串
        /// EX: Dictionary 轉換成 pid=1@nid=2
        /// </summary>
        /// <param name="sourceStr"></param>
        /// <returns></returns>
        string TranslatorToStr(SolventoDictionary<string, int> dic);
    }
}
