﻿using MvcPractice.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MvcPractice.Models.Services
{
    public class AccountService
    {
        UserRepository userRepository = new UserRepository();

        public void CreateNewUser(Users user)
        {
            Users Newuser = new Users();

            Newuser.Name = user.Name;
            Newuser.Email = user.Email;
            Newuser.Password = EncryptPassword(user.Password);

            userRepository.Insert(Newuser);
            userRepository.Dispose();
        }

        public Users GetExistedUser(string Useremail, string Userpwd)
        {
            Users ExistedUser = userRepository.Get(x => x.Email == Useremail & x.Password == Userpwd);

            return ExistedUser;
        }

        public Users GetUserById(int userid)
        {
            Users User = userRepository.Get(x => x.ID == userid);

            return User;
        }

        public bool CheckIfEmailRepeat(string email)
        {
            return userRepository.GetAll().Any(u => u.Email == email);
        }

        public bool CheckIfNameRepeat(string name)
        {
            return userRepository.GetAll().Any(u => u.Name == name);
        }
        
        public string EncryptPassword(string sDataIn)
        {
            MD5 md5Hasher = MD5.Create();

            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(sDataIn));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString().ToUpper();
        }
    }
}