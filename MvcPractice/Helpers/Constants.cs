﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Helpers
{
    public class Constants
    {
        /// <summary>
        /// WebGenie GIP 平台名稱
        /// </summary>
        public const string PlatForm = "MvcPractice";
        /// <summary>
        /// 系統核心WGID加密金鑰
        /// </summary>
        public const string EncrptKey = "solventosoft";

        /// <summary>
        /// 預設Kendo table頁面Size
        /// </summary>
        public const int DefaultPageSize = 10;

        public const string EncryptPid = "pid";
        public const string EncryptNid = "nid";
        public const string EncryptId = "id";
    }
}