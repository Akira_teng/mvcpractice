﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Models.FormBeans
{
    public class TodoListBean
    {
        public String name { get; set; }
        public int userID { get; set; }
        public List<Todos> todoThingsList { get; set; }
        public IPagedList<Todos> todoThingsPaged { get; set; }
    }
}