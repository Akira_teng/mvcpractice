﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Helpers
{
    public class SolventoDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public SolventoDictionary()
            : base()
        {

        }

        public SolventoDictionary(SolventoDictionary<TKey, TValue> SolventoDictionary)
            : base(SolventoDictionary)
        {

        }

        public new void Add(TKey key, TValue value)
        {
            if (base.ContainsKey(key))
            {
                base[key] = value;
            }
            else
            {
                base.Add(key, value);
            }
        }
        public new TValue this[TKey key]
        {
            get { return base.ContainsKey(key) ? base[key] : default(TValue); }
            set
            {
                if (base.ContainsKey(key))
                    base[key] = value;
                else
                    base.Add(key, value);
            }
        }
    }
}