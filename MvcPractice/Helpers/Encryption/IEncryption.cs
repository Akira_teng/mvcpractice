﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcPractice.Helpers.Encryption
{
    public interface IEncryption
    {
        /// <summary>
        /// 字串MD5加密
        /// </summary>
        /// <param name="sourceStr">要加密字串</param>
        /// <returns>加密字串</returns>
        string AesEncoding(string sourceStr);

        /// <summary>
        /// 字串MD5解密
        /// </summary>
        /// <param name="sourceStr">要解密字串</param>
        /// <returns>解密字串</returns>
        string AesDecoding(string sourceStr);
    }
}
