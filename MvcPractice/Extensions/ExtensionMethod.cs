﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using System.Web;
using System.Linq.Expressions;
using System.Linq;
using System.ComponentModel;
using System.Reflection;
using System.Web.Security;
using System.Security.Principal;
using System.Text;

namespace MvcPractice.Extensions
{
    public static partial class ExtensionMethod
    {
        /// <summary>
        /// 取得ModelState所有錯誤訊息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string GetErrorMessage(this ModelStateDictionary model)
        {
            try
            {
                var errors = model.Values.SelectMany(v => v.Errors).ToList();
                StringBuilder errorMessage = new StringBuilder();
                foreach (var item in errors)
                {
                    errorMessage.AppendFormat("{0}<br>", item.ErrorMessage);
                }
                return errorMessage.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "OrderBy");
        }
        public static IQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "OrderByDescending");
        }
        public static IQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "ThenBy");
        }
        public static IQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "ThenByDescending");
        }
        static IQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
        {
            string[] props = property.Split('.');
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(T), type)
                    .Invoke(null, new object[] { source, lambda });
            return (IOrderedQueryable<T>)result;
        }

        /// <summary>
        /// 判斷User是否有經過認證
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsUserIdentity(this IPrincipal User)
        {
            bool hasUser = User != null;
            bool isAuthenticated = hasUser && User.Identity.IsAuthenticated;
            bool isIdentity = isAuthenticated && User.Identity is FormsIdentity;

            return isIdentity;
        }
    }
}