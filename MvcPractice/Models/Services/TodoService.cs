﻿using MvcPractice.Models.FormBeans;
using MvcPractice.Models.Repositories;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Models.Services
{
    public class TodoService
    {
        TodosRepository todosRepository = new TodosRepository();
        UserRepository userRepository = new UserRepository();

        public TodoListBean GetUserTodoList(Users user)
        {
            TodoListBean todoList = new TodoListBean();

            var userId = userRepository.GetAll().Where(w => w.Email == user.Email).Select(s => s.ID).FirstOrDefault();
            var userName = userRepository.GetAll().Where(w => w.Email == user.Email).Select(s => s.Name).FirstOrDefault();
            var userTodoList = todosRepository.GetAll().Where(w => w.UserID == userId).OrderBy(o => o.UpdatedAt).ToList();

            todoList.name = userName;            
            todoList.userID = userId;
            todoList.todoThingsList = userTodoList;

            return todoList;
        }

        public void CreateNewTodoThing(String content, String completed, int userId)
        {
            Todos NewTodoThing = new Todos();

            NewTodoThing.UserID = userId;
            NewTodoThing.Content = content;
            NewTodoThing.UpdatedAt = DateTime.Now;
            if (completed.Equals("true"))
            {
                NewTodoThing.Completed = true;
            }
            if (completed.Equals("false"))
            {
                NewTodoThing.Completed = false;
            }

            todosRepository.Insert(NewTodoThing);
            todosRepository.Dispose();
        }

        public void EditTodoThing(String content, String completed, int id)
        {
            Todos todoThing = new Todos();

            todoThing.ID = id;
            todoThing.UserID = todosRepository.GetAll().Where(w => w.ID == id).Select(s => s.UserID).FirstOrDefault();
            todoThing.Content = content;
            todoThing.UpdatedAt = DateTime.Now;
            if (completed.Equals("true"))
            {
                todoThing.Completed = true;
            }
            if (completed.Equals("false"))
            {
                todoThing.Completed = false;
            }            
            
            todosRepository.Update(todoThing);
            todosRepository.Dispose();
        }

        public void DeleteTodoThing(int id)
        {
            todosRepository.Delete(todosRepository.GetAll().Where(w => w.ID == id).FirstOrDefault());
            todosRepository.Dispose();
        }
    }
}