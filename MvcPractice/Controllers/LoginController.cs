﻿using MvcPractice.Models;
using MvcPractice.Models.FormBeans;
using MvcPractice.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcPractice.Controllers
{
    public class LoginController : Controller
    {
        AccountService accountService = new AccountService();
        VaildationCodeService VaildationCodeService = new VaildationCodeService();

        public ActionResult Index()
        {
            Session["ValidateCode"] = VaildationCodeService.GetRandomCode();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginBean user)
        {
            var ExistedUser = accountService.GetExistedUser(user.Email, accountService.EncryptPassword(user.Password));
            if (ExistedUser != null && user.ValidateCode.Equals(Session["ValidateCode"].ToString()))
            {
                Session["userid"] = ExistedUser.ID;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.Remove("Email");
                ModelState.Remove("Password");
                ModelState.Remove("ValidateCode");
                Session["ValidateCode"] = VaildationCodeService.GetRandomCode();

                return View();
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Users user)
        {
            if (accountService.CheckIfEmailRepeat(user.Email))   
            {
                ModelState.AddModelError("email", "Email重複");
                return View(user);
            }

            if (accountService.CheckIfNameRepeat(user.Name))     
            {
                ModelState.AddModelError("name", "名字重複");
                return View(user);
            }

            accountService.CreateNewUser(user);

            return RedirectToAction("Index", "Login");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Login");
        }

        public FileContentResult CreateVaildationGraph(string id)
        {            
            return VaildationCodeService.CreatVerificationCodeGraph(Session["ValidateCode"].ToString()); 
        }

        public ActionResult ReloadVaildationCode()
        {
            Session.Clear();
            Session["ValidateCode"] = VaildationCodeService.GetRandomCode();

            return new EmptyResult();
        }
    }
}