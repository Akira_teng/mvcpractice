﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MvcPractice.Models.Services
{
    public class VaildationCodeService : Controller
    {
        public FileContentResult CreatVerificationCodeGraph(string vailstionCode)
        {
            FileContentResult vaildationGraph;
            int letterRotateAngle = 30; 
            Bitmap graph = new Bitmap( (int)(vailstionCode.Length * 20) , 30);  
            Graphics painter = Graphics.FromImage(graph);
            char[] chars = vailstionCode.ToCharArray();

            painter.Clear(Color.Pink);  
            
            //文字對齊
            StringFormat format = new StringFormat(StringFormatFlags.NoClip);
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;

            //文字顏色
            Color[] c = { Color.Black, Color.Red, Color.Blue, Color.Green, Color.Brown, Color.DarkBlue };                          

            //背景噪音線
            Random rand = new Random();
            for (int i = 0; i < 2; i++)
            {
                int x1 = rand.Next(10);
                int x2 = rand.Next(graph.Width - 10, graph.Width);
                int y1 = rand.Next(graph.Height);
                int y2 = rand.Next(graph.Height);

                painter.DrawLine(new Pen(c[rand.Next(6)]), x1, y1, x2, y2);
            }

            //文字特殊處理
            for (int i = 0; i < chars.Length; i++)
            {
                int cindex = rand.Next(6);
                int findex = rand.Next(5);
                Font f = new Font("Arial", 24, FontStyle.Regular);  
                Brush b = new SolidBrush(c[cindex]);
                Point dot = new Point(12, 16);
                float angle = rand.Next(-letterRotateAngle, letterRotateAngle);  
                painter.TranslateTransform(dot.X, dot.Y);  
                painter.RotateTransform(angle);
                painter.DrawString(chars[i].ToString(), f, b, 1, 1, format);
                painter.RotateTransform(-angle);  
                painter.TranslateTransform(2, -dot.Y);  
            }
            
            //生成圖片
            using (var memStream = new MemoryStream())
            {
                graph.Save(memStream, ImageFormat.Gif);
                vaildationGraph = this.File(memStream.GetBuffer(), "image/gif");
            }

            return vaildationGraph;

            //簡易版(陽春)驗證碼圖形
            //FileContentResult validationCode;            
            //Bitmap basemap = new Bitmap(117, 27, PixelFormat.Format24bppRgb); ;
            //Graphics graph = Graphics.FromImage(basemap); ;

            //graph.FillRectangle(new LinearGradientBrush(
            //    new Point(0, 0),
            //    new Point(117, 27),
            //    Color.FromArgb(210, 210, 210, 210),
            //    Color.FromArgb(210, 210, 210, 210))
            //    , 0, 0, 0, 27);

            //Font font = new Font(FontFamily.GenericSerif, 48, FontStyle.Bold, GraphicsUnit.Pixel);

            //graph.DrawString(vailstionCode, new Font("細明體", 16, FontStyle.Bold), new SolidBrush(Color.White), new PointF(3, 2));
            //graph.FillRectangle(new LinearGradientBrush(
            //                    new Point(0, 0),
            //                    new Point(117, 27),
            //                    Color.FromArgb(210, 210, 210, 210),
            //                    Color.FromArgb(210, 210, 210, 210))
            //                    , 0, 0, 0, 27);
            //graph.Save();               

            //using (var memStream = new System.IO.MemoryStream())
            //{
            //    basemap.Save(memStream, ImageFormat.Gif);
            //    validationCode = this.File(memStream.GetBuffer(), "image/gif");
            //}
            //return validationCode;
        }

        public string GetRandomCode()
        {
            Random random = new Random();
            string randomRange = "0123456789";
            string randomResult;
            StringBuilder sb = new StringBuilder();
            for (int word = 0; word < 4; word++)
            {
                randomResult = randomRange.Substring(random.Next(0, randomRange.Length - 1), 1);
                sb.Append(randomResult);
            }
            return sb.ToString();
        }
    }
}