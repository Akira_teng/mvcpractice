﻿using MvcPractice.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPractice.Models.Repositories
{
    public class UserRepository : GenericRepository<Users>, IUserRepository
    {
    }
}