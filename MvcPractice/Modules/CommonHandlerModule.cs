﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcPractice.Helpers.Encryption;

namespace MvcPractice.Modules
{
    public class CommonHandlerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(Encryption)).As(typeof(IEncryption));
            builder.RegisterType(typeof(SolventoEncryption)).As(typeof(ISolventoEncryption));
        }
    }
}